const mongoose = require('mongoose');
const config = require('config');
const logger = require('../lib/logger')();

module.exports = () => {
  const db = config.get('db');
  mongoose
    .connect(db)
    .then(() => logger.info(`Connected to ${db}...`))
    .catch((ex) => {
      logger.error(`Error connecting to ${db}... ${ex.message}`);
      process.exit(1);
    });
};

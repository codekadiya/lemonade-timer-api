const express = require('express');
const timers = require('../routes/timers');
const handleError = require('../middleware/error');

module.exports = (app) => {
  app.use(express.json());

  app.use('/timers', timers);

  // test webhook endpoint for manual testing using postman
  app.post('/webhook-test/:id', (req, res) => {
    res.send(`Test webhook response for "${req.params.id}"`);
  });

  app.use(handleError);
};

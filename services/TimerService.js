const axios = require('axios');
const { Timer } = require('../models/timer');
const logger = require('../lib/logger')();
const {
  getCurrentTimestamp,
  calculateInvocationTs,
  calculateTimeLeft,
} = require('../lib/time-util');
const { TIMER_STATUS } = require('../lib/constants');

class TimerService {
  constructor() {
    this.__pendingTimers = [];
    this.__scheduler = null;

    this.__loadTimers().then(() => {});
  }

  async saveTimer({ hours, minutes, seconds, url }) {
    const timer = new Timer({
      url,
      invokeAt: calculateInvocationTs(hours, minutes, seconds),
    });

    const fromDb = await timer.save();
    fromDb.id = fromDb._id.toString();
    logger.info(`Timer created {TimerId=${fromDb.id}}`);

    await this.__loadTimers();

    return fromDb;
  }

  async getTimerStatus(timer) {
    return {
      id: timer._id,
      timeLeft: calculateTimeLeft(timer.invokeAt),
    };
  }

  async __loadTimers() {
    const timers = await Timer.find({
      status: TIMER_STATUS.PENDING,
    });
    this.__pendingTimers = timers.map((timer) => ({
      id: timer._id.toString(),
      invokeAt: timer.invokeAt,
      url: timer.url,
    }));

    if (this.__pendingTimers.length > 0) {
      logger.info(`Pending timers count: ${this.__pendingTimers.length}`);
    }
  }

  start() {
    let _isRunning = false;
    this.__scheduler = setInterval(async () => {
      if (!_isRunning) {
        _isRunning = true;
        await this.__invokeWebhooks();
        _isRunning = false;
      }
    }, 500);

    logger.info(`Timers processing started...`);
  }

  stop() {
    clearInterval(this.__scheduler);
    this.__scheduler = null;

    logger.info(`Timers processing stopped.`);
  }

  async __invokeWebhooks() {
    const timersToInvoke = this.__pendingTimers.filter(
      (timer) => timer.invokeAt <= getCurrentTimestamp()
    );

    const successTimerIds = [];
    const errorTimerIds = [];
    for (const timer of timersToInvoke) {
      try {
        const response = await axios.post(`${timer.url}/${timer.id}`);
        successTimerIds.push(timer.id);
        logger.info(
          `Webhook invoked {TimerId=${timer.id}} [Response=${response.data}]`
        );
      } catch (e) {
        errorTimerIds.push(timer.id);
        logger.error(
          `Webhook invocation failure {TimerId=${timer.id}} [Error=${e.message}]`
        );
      }
    }

    if (successTimerIds.length > 0) {
      await this.__markTimersAsSuccess(successTimerIds);
    }

    if (errorTimerIds.length > 0) {
      await this.__markTimersAsError(errorTimerIds);
    }

    if (timersToInvoke.length > 0) {
      await this.__loadTimers();
    }
  }

  async __markTimersAsSuccess(timerIds) {
    await Promise.all(
      timerIds.map((timerId) =>
        Timer.findByIdAndUpdate(timerId, {
          $set: { status: TIMER_STATUS.SUCCESS },
        })
      )
    );
  }

  async __markTimersAsError(timerIds) {
    await Promise.all(
      timerIds.map((timerId) =>
        Timer.findByIdAndUpdate(timerId, {
          $set: { status: TIMER_STATUS.ERROR },
        })
      )
    );
  }
}

let instance = null;
const getTimerService = () => {
  if (!instance) {
    instance = new TimerService();
  }
  return instance;
};

module.exports = {
  getTimerService,
};

module.exports = {
  env: {
    es6: true,
    node: true,
  },
  extends: ['airbnb-base', 'prettier'],
  plugins: ['prettier'],
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  rules: {
    'prettier/prettier': 'error',
    'no-underscore-dangle': 'off',
    'consistent-return': 'off',
    'func-names': 'off',
    'class-methods-use-this': 'off',
    'no-restricted-syntax': 'off',
    'no-await-in-loop': 'off',
    'no-unused-vars': ['error', { argsIgnorePattern: 'next' }],
  },
  ignorePatterns: ['**/test/**/*.js'],
};

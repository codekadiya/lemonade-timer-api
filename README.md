# Lemonade Timer API

Lemonade Timer API is a REST API that allows its clients to get notified via webhooks after a specified time interval.

---
## Available Endpoints

### Timers
#### `POST /timers` - Set timer
```
curl --request POST 'http://localhost:3000/timers' \
--header 'Content-Type: application/json' \
--data-raw '{
    "hours": 0,
    "minutes": 0,
    "seconds": 50,
    "url": "http://localhost:3000/webhook-test"
}'
```
**Success Response:** 201

**Error Response:** 400

#### `GET /timers/{id}` - Get status of existing timer
```
curl --request GET 'http://localhost:3000/timers/627eaca351c63bfed2df17f1'
```
**Success Response:** 200

**Error Response:** 404

---
## Setup Instructions

Prerequisites: Node.js ([install](https://nodejs.org/en/download/)), MongoDB ([install](https://www.mongodb.com/docs/manual/installation/#mongodb-installation-tutorials))

1. Clone repository `git clone https://gitlab.com/codekadiya/lemonade-timer-api.git`
2. Run command `npm install`
3. Set environment variables *if required to override the defaults* (optional)
- `lemonadetimers_port` - API server port
e.g. `export lemonadetimers_port=3000`
- `lemonadetimers_db` - MongoDB connection string
e.g. `export lemonadetimers_db=mongodb://localhost/lemonade_timers_dev`

Note: defaults are configured in `/config` for development and test environments

---
## Start API Server

1. Run command `npm start` and wait for logs to appear
```
{"level":30,"time":1652469210732,"pid":27089,"hostname":"xyz.local","msg":"Lemonade Timer API listening at http://localhost:3000"}
{"level":30,"time":1652469210740,"pid":27089,"hostname":"xyz.local","msg":"Connected to mongodb://localhost/lemonade_timers_dev..."}
```

Now the server is ready to accept requests from clients.

---
## Postman

1. Make sure the API Server is running
2. Import the [postman collection](postman_collection.json) to Postman
3. Populate the following collection variables in the "CURRENT VALUE" column
- `base_url` - API Base URL e.g. http://localhost:3000
4. Save collection
5. Run collection

Ensure success responses are returned for all requests.

## Run Unit and Functional Tests

1. Run command `npm test` and wait for all tests to execute
2. Once test suites are completed, the code coverage summary will be printed on the console

![test_summary](https://imgur.com/4jdUufL.png)

3. Open `coverage/lcov-report/index.html` in browser to read the detailed coverage report

![coverage_report_dir](https://imgur.com/qcB1KsE.png)

![coverage_report_html](https://imgur.com/sfDPgLA.png)

## Code Structure

* `/config` - configuration files for each deployment environment
* `/lib` - functions, utilities and constants commonly used across the application
* `/middleware` - reusable request handling logic
* `/models` - database schema and models
* `/routes` - requests and responses handling logic
* `/services` - core business logic
* `/test` - unit and functional tests
* `index.js` - entry point and server instantiation

## Dependency Usage

* [axios](https://www.npmjs.com/package/axios) - remote HTTP request invocation
* [config](https://www.npmjs.com/package/config) - environment configuration management
* [express](https://www.npmjs.com/package/express) - nodejs web framework
* [joi](https://www.npmjs.com/package/joi) - request payload validation
* [mongoose](https://www.npmjs.com/package/mongoose) - mongodb object data modeling
* [pino](https://www.npmjs.com/package/pino) - application logging
* [eslint](https://www.npmjs.com/package/eslint) - static code analysis
* [prettier](https://www.npmjs.com/package/prettier) - code formatting
* [pre-commit](https://www.npmjs.com/package/pre-commit) - git pre-commit hooks configuration
* [jest](https://www.npmjs.com/package/jest) - unit testing and code coverage
* [supertest](https://www.npmjs.com/package/supertest) - functional testing over HTTP

---

For any questions feel free to contact Subhash Vithanapathirana <codekadiya@gmail.com>

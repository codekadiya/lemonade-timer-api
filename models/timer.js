const mongoose = require('mongoose');
const Joi = require('joi');
const { TIMER_STATUS } = require('../lib/constants');

const timerSchema = new mongoose.Schema({
  invokeAt: {
    type: Number,
    required: true,
  },
  url: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    enum: [TIMER_STATUS.PENDING, TIMER_STATUS.SUCCESS, TIMER_STATUS.ERROR],
    default: TIMER_STATUS.PENDING,
  },
});

timerSchema.index({ status: 1 });

const Timer = mongoose.model('Timer', timerSchema);

function validate(timer) {
  const schema = {
    hours: Joi.number().min(0).required(),
    minutes: Joi.number().min(0).max(59).required(),
    seconds: Joi.number().min(0).max(59).required(),
    url: Joi.string().uri().required(),
  };

  return Joi.object(schema).validate(timer);
}

module.exports = {
  Timer,
  validate,
};

const request = require('supertest');
const mongoose = require('mongoose');
const axios = require('axios');
const { Timer } = require('../../models/timer');
const initServer = require('../../index');

jest.mock('axios');

let server;

afterAll((done) => {
  mongoose.connection.close();
  done();
});

beforeEach(() => {
  server = initServer();
});

afterEach(async () => {
  await Timer.deleteMany({});
  await server.close();
});

it('functional test: webhook invocation', async () => {
  axios.post.mockImplementation(() => Promise.resolve({ data: 'hello' }));

  // Set timer for 1 second
  let response = await request(server).post('/timers').send({
    hours: 0,
    minutes: 0,
    seconds: 1,
    url: 'https://someserver-1sec.com',
  });
  const timerId1Second = response.body.id;

  // Set timer for 3 seconds
  response = await request(server).post('/timers').send({
    hours: 0,
    minutes: 0,
    seconds: 3,
    url: 'https://someserver-3sec.com',
  });
  const timerId3Seconds = response.body.id;

  // Set timer for 10 seconds
  response = await request(server).post('/timers').send({
    hours: 0,
    minutes: 0,
    seconds: 10,
    url: 'https://someserver-10sec.com',
  });
  const timerId10Seconds = response.body.id;

  // Wait for 2 seconds
  await new Promise((r) => setTimeout(r, 2000));

  // Verify webhook url for 1 second timer has been invoked on scheduled time
  expect(axios.post).toHaveBeenCalledWith(
    `https://someserver-1sec.com/${timerId1Second}`
  );

  // Wait for 2 seconds
  await new Promise((r) => setTimeout(r, 2000));

  // Verify webhook url for 3 second timer has been invoked on scheduled time
  expect(axios.post).toHaveBeenCalledWith(
    `https://someserver-3sec.com/${timerId3Seconds}`
  );

  // Verify webhook url for 10 second timer has NOT been invoked
  expect(axios.post).not.toHaveBeenCalledWith(
    `https://someserver-10sec.com/${timerId10Seconds}`
  );
});

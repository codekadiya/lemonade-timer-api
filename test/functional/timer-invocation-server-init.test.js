const request = require('supertest');
const mongoose = require('mongoose');
const axios = require('axios');
const { Timer } = require('../../models/timer');
const initServer = require('../../index');

jest.mock('axios');

let server;

afterAll((done) => {
  mongoose.connection.close();
  done();
});

beforeEach(() => {
  server = initServer();
});

afterEach(async () => {
  await Timer.deleteMany({});
  await server.close();
});

it('functional test: webhook invocation on server initialization', async () => {
  axios.post.mockImplementation(() => Promise.resolve({ data: 'hello' }));

  // Set timer for 2 seconds
  let response = await request(server).post('/timers').send({
    hours: 0,
    minutes: 0,
    seconds: 2,
    url: 'https://someserver.com',
  });
  const timerId = response.body.id;

  // Shut down the server
  await server.close();

  // Wait for 3 seconds
  await new Promise((r) => setTimeout(r, 3000));

  // Restart the server
  server = initServer();

  // Wait for 1 second
  await new Promise((r) => setTimeout(r, 1000));

  // Verify webhook url has been invoked after server is up
  expect(axios.post).toHaveBeenCalledWith(`https://someserver.com/${timerId}`);
});

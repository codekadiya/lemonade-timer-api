const request = require('supertest');
const mongoose = require('mongoose');
const { Timer } = require('../../models/timer');
const initServer = require('../../index');

let server;

afterAll((done) => {
  mongoose.connection.close();
  done();
});

beforeEach(() => {
  server = initServer();
});

afterEach(async () => {
  await Timer.deleteMany({});
  await server.close();
});

it('functional test: timer endpoints happy path', async () => {
  // Set timer for 3661 seconds
  let response = await request(server).post('/timers').send({
    hours: 1,
    minutes: 1,
    seconds: 1,
    url: 'https://someserver.com',
  });
  expect(response.status).toBe(201);
  expect(response.body.id).toBeDefined();
  const timerId = response.body.id;

  // Get timer status and verify time left is accurate
  response = await request(server).get(`/timers/${timerId}`).send();
  expect(response.status).toBe(200);
  expect(response.body.id).toBe(timerId);
  expect(response.body.time_left).toBeGreaterThanOrEqual(3660);

  const timeLeft = response.body.time_left;

  // Wait for one second
  await new Promise((r) => setTimeout(r, 1000));

  // Get timer status again to verify time left has now reduced
  response = await request(server).get(`/timers/${timerId}`).send();
  expect(response.body.time_left).toBeLessThan(timeLeft);
});

it('functional test: timer endpoints validations', async () => {
  // Provide invalid id for get timer status
  response = await request(server).get(`/timers/1`).send();
  expect(response.status).toBe(400);

  // Provide non-existing id for get timer status
  response = await request(server)
    .get(`/timers/627ea03153153d511dd66315`)
    .send();
  expect(response.status).toBe(404);

  // Provide empty request body to set timer
  response = await request(server).post(`/timers`).send({});
  expect(response.status).toBe(400);
});

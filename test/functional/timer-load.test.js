const request = require('supertest');
const mongoose = require('mongoose');
const axios = require('axios');
const { Timer } = require('../../models/timer');
const initServer = require('../../index');

jest.mock('axios');

let server;

afterAll((done) => {
  mongoose.connection.close();
  done();
});

beforeEach(() => {
  server = initServer();
});

afterEach(async () => {
  await Timer.deleteMany({});
  await server.close();
});

it('functional test: webhook invocation load test', async () => {
  axios.post.mockImplementation(() => Promise.resolve({ data: 'hello' }));

  // Set 100 timers for 1 second
  const setTimerRequests = [];
  for (let i = 1; i <= 100; i++) {
    setTimerRequests.push(
      request(server).post('/timers').send({
        hours: 0,
        minutes: 0,
        seconds: 1,
        url: 'https://someserver.com',
      })
    );
  }
  const setTimerResponses = await Promise.all(setTimerRequests);
  const timerIds = setTimerResponses.map((response) => response.body.id);

  // Wait for 2 seconds
  await new Promise((r) => setTimeout(r, 2000));

  // Verify webhook url for all 100 timers is invoked
  timerIds.forEach((timerId) => {
    expect(axios.post).toHaveBeenCalledWith(
      `https://someserver.com/${timerId}`
    );
  });
});

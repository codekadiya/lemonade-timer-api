const {
  getCurrentTimestamp,
  calculateInvocationTs,
  calculateTimeLeft,
} = require('../../../lib/time-util');

describe('getCurrentTimestamp()', () => {
  it('should return current timestamp in seconds', () => {
    const result = getCurrentTimestamp();
    expect(result).toBe(Math.round(Date.now() / 1000));
  });
});

describe('calculateInvocationTs()', () => {
  it('should return timestamp to invoke webhook', () => {
    const result = calculateInvocationTs(1, 1, 1);
    expect(result).toBe(getCurrentTimestamp() + 3661);
  });
});

describe('calculateTimeLeft()', () => {
  it('should return number of seconds remaning to invoke webhook from current time (future)', () => {
    const result = calculateTimeLeft(getCurrentTimestamp() + 3661);
    expect(result).toBe(3661);
  });

  it('should return number of seconds remaning to invoke webhook from current time (past)', () => {
    const result = calculateTimeLeft(getCurrentTimestamp() - 1);
    expect(result).toBe(0);
  });
});

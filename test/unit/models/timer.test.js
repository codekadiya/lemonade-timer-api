const { validate } = require('../../../models/timer');

describe('timer.validate', () => {
  it('should pass validation if timer object is valid', () => {
    const result = validate({
      hours: 1,
      minutes: 1,
      seconds: 1,
      url: 'https://someserver.com',
    });
    expect(result).not.toHaveProperty('error');
  });

  it('should fail validation if timer object has invalid hours', () => {
    const result = validate({
      hours: -1,
      minutes: 1,
      seconds: 1,
      url: 'https://someserver.com',
    });
    expect(result).toHaveProperty('error');
    expect(result.error.details[0].message).toContain('hours');
  });

  it('should fail validation if timer object has invalid minutes', () => {
    const result = validate({
      hours: 1,
      minutes: 60,
      seconds: 1,
      url: 'https://someserver.com',
    });
    expect(result).toHaveProperty('error');
    expect(result.error.details[0].message).toContain('minutes');
  });

  it('should fail validation if timer object has invalid seconds', () => {
    const result = validate({
      hours: 1,
      minutes: 1,
      seconds: 60,
      url: 'https://someserver.com',
    });
    expect(result).toHaveProperty('error');
    expect(result.error.details[0].message).toContain('seconds');
  });

  it('should fail validation if timer object has invalid url', () => {
    const result = validate({
      hours: 1,
      minutes: 1,
      seconds: 1,
      url: 'someserver.com',
    });
    expect(result).toHaveProperty('error');
    expect(result.error.details[0].message).toContain('url');
  });
});

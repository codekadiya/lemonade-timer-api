const error = require('../../../middleware/error');

describe('error middleware', () => {
  it('should return 500', () => {
    const err = new Error('Sample Error');

    const req = {};

    const mockSend = jest.fn();
    const mockStatus = jest.fn().mockReturnValue({
      send: mockSend,
    });
    const res = {
      status: mockStatus,
    };

    const next = jest.fn();

    error(err, req, res, next);

    expect(mockStatus.mock.calls.length).toBe(1);
    expect(mockStatus.mock.calls[0][0]).toBe(500);

    expect(mockSend.mock.calls.length).toBe(1);
    expect(mockSend.mock.calls[0][0].error).toBe('Something went wrong.');
  });
});

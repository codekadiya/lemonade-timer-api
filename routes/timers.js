const express = require('express');
const checkTimerExists = require('../middleware/timer/checkExists');
const validate = require('../middleware/timer/validate');
const validateId = require('../middleware/validateId');
const { HTTP_RESPONSE_CODES } = require('../lib/constants');
const { getTimerService } = require('../services/TimerService');

const router = express.Router();
const timerService = getTimerService();

router.post('/', validate, async (req, res) => {
  const { id } = await timerService.saveTimer(req.body);
  res.status(HTTP_RESPONSE_CODES.CREATED).send({ id });
});

router.get('/:id', [validateId, checkTimerExists], async (req, res) => {
  const { id, timeLeft } = await timerService.getTimerStatus(req.timer);
  res.send({ id, time_left: timeLeft });
});

module.exports = router;

const express = require('express');
require('express-async-errors');
const config = require('config');
const logger = require('./lib/logger')();
const { getTimerService } = require('./services/TimerService');

const app = express();

require('./startup/db')();
require('./startup/routes')(app);

function initServer() {
  const port = config.get('port');

  let timerService;
  const server = app.listen(port, () => {
    logger.info(`Lemonade Timer API listening at http://localhost:${port}`);

    timerService = getTimerService();
    timerService.start();
  });

  server.on('close', () => {
    timerService.stop();
  });

  return server;
}

if (process.env.NODE_ENV !== 'test') {
  initServer();
}

module.exports = initServer;

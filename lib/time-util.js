function getCurrentTimestamp() {
  return Math.round(Date.now() / 1000);
}

function calculateInvocationTs(hours, minutes, seconds) {
  return getCurrentTimestamp() + hours * 60 * 60 + minutes * 60 + seconds;
}

function calculateTimeLeft(invokeAt) {
  const timeLeft = invokeAt - getCurrentTimestamp();
  return timeLeft > 0 ? timeLeft : 0;
}

module.exports = {
  getCurrentTimestamp,
  calculateInvocationTs,
  calculateTimeLeft,
};

const pino = require('pino');

function getLogger(opts = {}) {
  return pino(opts);
}

module.exports = getLogger;

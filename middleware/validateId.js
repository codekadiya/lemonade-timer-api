const mongoose = require('mongoose');
const { ERROR_CODES, HTTP_RESPONSE_CODES } = require('../lib/constants');

module.exports = (req, res, next) => {
  if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
    return res
      .status(HTTP_RESPONSE_CODES.BAD_REQUEST)
      .send({ code: ERROR_CODES.INVALID_ID, error: 'Invalid ID.' });
  }

  next();
};

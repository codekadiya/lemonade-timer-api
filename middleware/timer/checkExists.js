const { Timer } = require('../../models/timer');
const { ERROR_CODES, HTTP_RESPONSE_CODES } = require('../../lib/constants');

module.exports = async (req, res, next) => {
  const timer = await Timer.findById(req.params.id);
  if (!timer) {
    return res.status(HTTP_RESPONSE_CODES.NOT_FOUND).send({
      code: ERROR_CODES.TIMER_NOT_FOUND,
      error: 'Timer not found.',
    });
  }
  req.timer = timer;
  next();
};

const { validate } = require('../../models/timer');
const { ERROR_CODES, HTTP_RESPONSE_CODES } = require('../../lib/constants');

module.exports = async (req, res, next) => {
  const { error } = validate(req.body);
  if (error) {
    return res.status(HTTP_RESPONSE_CODES.BAD_REQUEST).send({
      code: ERROR_CODES.VALIDATION_FAILED,
      error: error.details[0].message,
    });
  }
  next();
};

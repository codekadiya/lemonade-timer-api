const { ERROR_CODES, HTTP_RESPONSE_CODES } = require('../lib/constants');

module.exports = (err, req, res, next) => {
  res
    .status(HTTP_RESPONSE_CODES.INTERNAL_SERVER_ERROR)
    .send({ code: ERROR_CODES.SERVER_ERROR, error: 'Something went wrong.' });
};
